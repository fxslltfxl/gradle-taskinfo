/*
 * Copyright 2020 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.util.List;
import javax.annotation.Nonnull;

import org.barfuin.gradle.taskinfo.util.TaskNodeHolder;
import org.gradle.api.Project;


/**
 * Helper class for setting up some test fixtures.
 */
public final class Fixtures
{
    private Fixtures()
    {
        // utility class
    }



    public static TaskProbe buildTaskProbe(@Nonnull final Project pProject, final boolean pIsColor,
        final List<TaskNodeHolder> pExecutionQueue)
    {
        return new TaskProbe(pProject, pIsColor, pExecutionQueue);
    }
}
