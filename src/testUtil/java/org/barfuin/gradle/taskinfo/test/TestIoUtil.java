/*
 * Copyright 2020 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import javax.annotation.Nonnull;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;


/**
 * Read text from a file or write it to a file.
 */
public final class TestIoUtil
{
    private TestIoUtil()
    {
        // utility class
    }



    @Nonnull
    public static String readFileAndClose(final InputStream pInputStream)
    {
        try {
            final byte[] fileBytes = IOUtils.toByteArray(pInputStream);
            String result = new String(fileBytes, StandardCharsets.UTF_8).trim();
            return LogXformUtil.unixifyLineBreaks(result);
        }
        catch (IOException | RuntimeException e) {
            Assert.fail("Failed to read file during functional test: " + e.getMessage());
            return null;
        }
        finally {
            IOUtils.closeQuietly(pInputStream, null);
        }
    }



    @SuppressWarnings("unused")
    public static void string2File(final String pString, final String pFilename)
        throws IOException
    {
        Files.write(Paths.get(pFilename), pString.getBytes(StandardCharsets.UTF_8),
            StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
    }
}
