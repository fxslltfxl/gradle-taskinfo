In order to execute [:producer:test, :test], the following tasks would be executed in this order:

[33m  1.[m [90m:workNodeAction_xxxxxxxxx[m      [90m(org.gradle.execution.plan.ActionNode)[m
[33m  2.[m :producer:compileJava          [90m(org.gradle.api.tasks.compile.JavaCompile)[m
[33m  3.[m :producer:processResources     [90m(org.gradle.language.jvm.tasks.ProcessResources)[m
[33m  4.[m :producer:classes              [90m(org.gradle.api.DefaultTask)[m
[33m  5.[m :producer:jar                  [90m(org.gradle.api.tasks.bundling.Jar)[m
[33m  6.[m [90m:transformationStep_xxxxxxxxx[m  [90m(org.gradle.api.internal.artifacts.transform.TransformationNode$InitialTransformationNode)[m
[33m  7.[m :compileJava                   [90m(org.gradle.api.tasks.compile.JavaCompile)[m
[33m  8.[m :processResources              [90m(org.gradle.language.jvm.tasks.ProcessResources)[m
[33m  9.[m :classes                       [90m(org.gradle.api.DefaultTask)[m
[33m 10.[m :compileTestJava               [90m(org.gradle.api.tasks.compile.JavaCompile)[m
[33m 11.[m :processTestResources          [90m(org.gradle.language.jvm.tasks.ProcessResources)[m
[33m 12.[m :testClasses                   [90m(org.gradle.api.DefaultTask)[m
[33m 13.[m :test                          [90m(org.gradle.api.tasks.testing.Test)[m
[33m 14.[m :producer:compileTestJava      [90m(org.gradle.api.tasks.compile.JavaCompile)[m
[33m 15.[m :producer:processTestResources [90m(org.gradle.language.jvm.tasks.ProcessResources)[m
[33m 16.[m :producer:testClasses          [90m(org.gradle.api.DefaultTask)[m
[33m 17.[m :producer:test                 [90m(org.gradle.api.tasks.testing.Test)[m

Items in [90mgray[m represent nodes which are not backed by tasks but by transformation or action nodes added by Gradle.
:tiOrder
