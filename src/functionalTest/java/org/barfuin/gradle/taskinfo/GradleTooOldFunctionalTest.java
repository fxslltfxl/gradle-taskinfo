/*
 * Copyright 2020 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;

import org.barfuin.gradle.taskinfo.test.LogXformUtil;
import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;


/**
 * Tests that we properly complain if the Gradle version we're run on is too old.
 */
public class GradleTooOldFunctionalTest
{
    @Rule
    public final TemporaryFolder testProjectDir = new TemporaryFolder();



    @Test
    public void testGradleTooOld()
        throws IOException
    {
        final File projectDir = testProjectDir.getRoot();
        Files.createDirectories(projectDir.toPath());
        writeString(new File(projectDir, "settings.gradle"), "");
        writeString(new File(projectDir, "build.gradle"),
            "plugins {"
                + "  id('org.barfuin.gradle.taskinfo')"
                + "}");

        GradleRunner runner = GradleRunner.create();
        runner.forwardOutput();
        runner.withPluginClasspath();
        runner.withGradleVersion("3.3");    // one older than our minimum version
        runner.withArguments("--info", "--stacktrace", "tiTree", "tasks");
        runner.withProjectDir(projectDir);
        runner.withDebug(true);
        BuildResult buildResult = runner.buildAndFail();
        String outputLog = LogXformUtil.unixifyLineBreaks(buildResult.getOutput());

        Assert.assertTrue(outputLog.contains("* What went wrong:\n"
            + "The 'tiTree' task defined by the 'org.barfuin.gradle.taskinfo' plugin "
            + "requires at least Gradle 3.4 to be run."));
    }



    private void writeString(final File pFile, final String pString)
        throws IOException
    {
        try (Writer writer = new FileWriter(pFile)) {
            writer.write(pString);
        }
    }
}
