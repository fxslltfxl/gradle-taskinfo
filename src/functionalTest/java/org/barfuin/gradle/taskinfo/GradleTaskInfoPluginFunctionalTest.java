/*
 * Copyright 2020 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.barfuin.gradle.taskinfo.test.LogXformUtil;
import org.barfuin.gradle.taskinfo.test.TestIoUtil;
import org.gradle.util.GradleVersion;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.tools4j.spockito.Spockito;


/**
 * Functional tests of the gradle-taskinfo plugin.
 */
@Spockito.Unroll({
    "| GradleVersion |",
    "|---------------|",
    "|           3.4 |",
    "|         3.5.1 |",
    "|        4.10.2 |",
    "|         5.6.4 |",
    "|         6.2.2 |",
    "|           6.9 |",
    "|           7.0 |"
})
@RunWith(Spockito.class)
public class GradleTaskInfoPluginFunctionalTest
{
    /** The first version of Gradle that could perform deferred configuration */
    private static final GradleVersion GRADLE_49 = GradleVersion.version("4.9-rc-1");

    /**
     * The first version of Gradle that knew about artifact transforms and
     * {@link org.gradle.api.artifacts.transform.InputArtifact @InputArtifact} annotations
     */
    private static final GradleVersion GRADLE_53 = GradleVersion.version("5.3");

    /** The first version of Gradle where TransformationNodes have an "owning project" */
    private static final GradleVersion GRADLE_60 = GradleVersion.version("6.0.0-rc-1");

    /** The first version of Gradle that creates extra work node actions for transformation nodes */
    private static final GradleVersion GRADLE_67 = GradleVersion.version("6.7");

    @Rule
    public final TemporaryFolder testProjectDir = new TemporaryFolder();

    @Spockito.Ref
    @SuppressWarnings({"unused", "RedundantSuppression"})
    private String gradleVersion;

    private Fixtures fixtures = null;



    @Before
    public void beforeTest()
    {
        fixtures = new Fixtures(testProjectDir.getRoot(), gradleVersion);
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void canRunUnrelatedTask()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "unrelated1");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        Assert.assertTrue(buildLog.contains("Hello world from the unrelated task 1."));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void canRunUnrelatedTask2()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "unrelated1", "unrelated2");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        Assert.assertTrue(buildLog.contains("Hello world from the unrelated task 1."));
        Assert.assertTrue(buildLog.contains("Hello world from the unrelated task 2."));
        Assert.assertFalse(
            buildLog.contains("WARNING: More than one task specified as argument of task info [:unrelated"));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void simpleTest()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "tiTree", "taskA");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(getClass().getResourceAsStream("expected-tree1.txt"));
        Assert.assertTrue(buildLog.contains(expected));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void simpleTestReverseInput()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "taskA", "tiTree");   // tiTree at end
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(getClass().getResourceAsStream("expected-tree1.txt"));
        Assert.assertTrue(buildLog.contains(expected));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testJsonOutput()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        fixtures.act(true, "tiJson", "taskA");

        final String expected = TestIoUtil.readFileAndClose(getClass().getResourceAsStream("expected-tree1.json"));
        final String actual = TestIoUtil.readFileAndClose(new FileInputStream(new File(testProjectDir.getRoot(),
            "build/taskinfo/taskinfo-taskA.json")));
        Assert.assertEquals(expected, actual);
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testJsonOutputReverseInput()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        fixtures.act(true, "taskA", "tiJson");   // tiJson at end

        final String expected = TestIoUtil.readFileAndClose(getClass().getResourceAsStream("expected-tree1.json"));
        final String actual = TestIoUtil.readFileAndClose(new FileInputStream(new File(testProjectDir.getRoot(),
            "build/taskinfo/taskinfo-taskA.json")));
        Assert.assertEquals(expected, actual);
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testOrderedOutput()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "tiOrder", "taskA");
        fixtures.assertAnsiEscape(buildLog, true);
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(getClass().getResourceAsStream("expected-tree1-order.txt"));
        Assert.assertTrue(buildLog.contains(expected));
    }



    /**
     * Test that at least one entry node is given other than our own tasks. This means also that it is not currently
     * possible to run tiTree on a tiTree task.
     *
     * @throws IOException failed to set up test case
     */
    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testMissingEntryNode()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(false, "tiTree", "tiTree");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        Assert.assertTrue(buildLog.contains("GradleException: No task specified for tiTree"));
    }



    /**
     * Test that when more than one possible entry node is specified, we use the first.
     *
     * @throws IOException failed to set up test case
     */
    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testMultipleEntryNodes()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "tiTree", "taskB", "taskE");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        Assert.assertTrue(buildLog.contains(
            "WARNING: More than one task specified as argument of tiTree [:taskB, :taskE]. Will report on ':taskB'."));
        Assert.assertTrue(buildLog.contains("\n:taskB (org.gradle.api.DefaultTask)\n"));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testOrderedNoDependencies()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "tiOrder", "taskB");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        Assert.assertTrue(buildLog.contains(adjustExpectation(
            "1. :taskB (org.gradle.api.DefaultTask)\n\n:tiOrder", "tiOrder")));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void simpleTest2()
        throws IOException
    {
        fixtures.arrangeTestProject("setup2");

        String buildLog = fixtures.act(true, "tiTree", "taskA");

        fixtures.assertAnsiEscape(buildLog, true);
        buildLog = LogXformUtil.sanitizeLog(buildLog);
        final String expected = adjustExpectation(TestIoUtil.readFileAndClose(
            getClass().getResourceAsStream("expected-tree2.txt")), "tiTree");
        Assert.assertTrue(buildLog.contains(expected));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testPruneRepeatingSubtrees()
        throws IOException
    {
        fixtures.arrangeTestProject("setup2");

        String buildLog = fixtures.act(true, "-Ptaskinfo.clipped=true", "tiTree", "taskA");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = adjustExpectation(TestIoUtil.readFileAndClose(
            getClass().getResourceAsStream("expected-tree2-clipped.txt")), "tiTree");
        Assert.assertTrue(buildLog.contains(expected));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testColorTurnedOff()
        throws IOException
    {
        fixtures.arrangeTestProject("setup2");

        String buildLog = fixtures.act(true, "-Ptaskinfo.color=false", "-Ptaskinfo.clipped=true", "tiTree", "taskA");

        fixtures.assertAnsiEscape(buildLog, false);
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = adjustExpectation(TestIoUtil.readFileAndClose(
            getClass().getResourceAsStream("expected-tree2-clipped.txt")), "tiTree");
        Assert.assertTrue(buildLog.contains(expected));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testColorTurnedOffOrdered()
        throws IOException
    {
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "-Ptaskinfo.color=false", "tiOrder", "taskA");

        fixtures.assertAnsiEscape(buildLog, false);
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(getClass().getResourceAsStream("expected-tree1-order.txt"));
        Assert.assertTrue(buildLog.contains(expected));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testCyclicDependencyDetection()
        throws IOException
    {
        fixtures.arrangeTestProject("setup3");

        String buildLog = fixtures.act(true, "tiTree", "taskA");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        Assert.assertTrue(buildLog.contains(adjustExpectation(
            ":taskA                       (org.gradle.api.DefaultTask)\n"
                + "`--- :taskB                  (org.gradle.api.DefaultTask)\n"
                + "     `--- :taskA (finalizer) <shown before>\n"
                + "\n:tiTree", "tiTree")));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testMultiTaskOrdered()
        throws IOException
    {
        fixtures.arrangeTestProject("setup2");

        String buildLog = fixtures.act(true, "tiOrder", "taskB", "taskD");   // multiple args OK for tiOrder
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = adjustExpectation(TestIoUtil.readFileAndClose(
            getClass().getResourceAsStream("expected-tree2-order-multi.txt")), "tiOrder");
        Assert.assertTrue(buildLog.contains(expected));
        Assert.assertFalse(buildLog.contains("WARNING: More than one task"));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testMultiTaskOrderedReversed()
        throws IOException
    {
        fixtures.arrangeTestProject("setup2");

        String buildLog = fixtures.act(true, "taskB", "taskD", "tiOrder");   // tiOrder at end
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = adjustExpectation(TestIoUtil.readFileAndClose(
            getClass().getResourceAsStream("expected-tree2-order-multi.txt")), "tiOrder");
        Assert.assertTrue(buildLog.contains(expected));
        Assert.assertFalse(buildLog.contains("WARNING: More than one task"));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testMultiTaskOrderedJumbled()
        throws IOException
    {
        fixtures.arrangeTestProject("setup2");

        String buildLog = fixtures.act(true, "taskB", "tiOrder", "taskD");   // tiOrder in middle position
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = adjustExpectation(TestIoUtil.readFileAndClose(
            getClass().getResourceAsStream("expected-tree2-order-multi.txt")), "tiOrder");
        Assert.assertTrue(buildLog.contains(expected));
        Assert.assertFalse(buildLog.contains("WARNING: More than one task"));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testMultiProjectBuildTaskMatching()
        throws IOException
    {
        fixtures.arrangeTestProject("setup4");

        String buildLog = fixtures.act(true, "tiTree", "taskA");   // matches 3 tasks in reality!
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = adjustExpectation(TestIoUtil.readFileAndClose(
            getClass().getResourceAsStream("expected-tree4.txt")), "tiTree");
        Assert.assertTrue(buildLog.contains(expected));
        Assert.assertTrue(buildLog.contains("WARNING: More than one task"));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testDeferredConfiguration()
        throws IOException
    {
        Assume.assumeThat(GradleVersion.version(gradleVersion), Fixtures.isGreaterThanOrEqualTo(GRADLE_49));

        fixtures.arrangeTestProject("setup5");

        String buildLog = fixtures.act(true, "tiTree", "taskA");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = ":taskA           (org.gradle.api.DefaultTask)\n"
            + "+--- :taskB      (org.gradle.api.DefaultTask)\n"
            + "|    `--- :taskC (org.gradle.api.DefaultTask)\n"
            + "`--- :taskD      (org.gradle.api.DefaultTask)\n"
            + "\n"
            + ":tiTree";
        Assert.assertTrue(buildLog.contains(expected));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testDeferredConfigurationOrder()
        throws IOException
    {
        Assume.assumeThat(GradleVersion.version(gradleVersion), Fixtures.isGreaterThanOrEqualTo(GRADLE_49));

        fixtures.arrangeTestProject("setup5");

        String buildLog = fixtures.act(true, "tiOrder", "taskA");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = "In order to execute [:taskA], the following tasks would be executed in this order:\n"
            + "\n"
            + "  1. :taskC (org.gradle.api.DefaultTask)\n"
            + "  2. :taskB (org.gradle.api.DefaultTask)\n"
            + "  3. :taskD (org.gradle.api.DefaultTask)\n"
            + "  4. :taskA (org.gradle.api.DefaultTask)\n"
            + "\n"
            + ":tiOrder";
        Assert.assertTrue(buildLog.contains(expected));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testTransformNodesTreeColored()
        throws IOException
    {
        testTransformNodesTree(true);
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testTransformNodesTreePlain()
        throws IOException
    {
        testTransformNodesTree(false);
    }



    private void testTransformNodesTree(final boolean pColorEnabled)
        throws IOException
    {
        Assume.assumeThat(GradleVersion.version(gradleVersion), Fixtures.isGreaterThanOrEqualTo(GRADLE_53));

        fixtures.arrangeTestProject("setup6-transform");

        String buildLog = fixtures.act(true, "-Ptaskinfo.color=" + pColorEnabled, "tiTree", "test");
        buildLog = LogXformUtil.normalizeAddresses(LogXformUtil.unixifyLineBreaks(buildLog));

        String expected = TestIoUtil.readFileAndClose(getStreamFor(
            "expected-tree6-transform" + (pColorEnabled ? "-color" : "-plain") + ".txt"));
        if (GradleVersion.version(gradleVersion).compareTo(GRADLE_60) < 0) {
            expected = expected.replace(":transformationStep_xxxxxxxxx", "transformationStep_xxxxxxxxx ");
        }
        Assert.assertTrue(buildLog.contains(expected));
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testTransformNodesOrderColored()
        throws IOException
    {
        testTransformNodesOrder(true);
    }



    @Test
    @Spockito.Name("[Gradle {GradleVersion}]")
    public void testTransformNodesOrderPlain()
        throws IOException
    {
        testTransformNodesOrder(false);
    }



    private void testTransformNodesOrder(final boolean pColorEnabled)
        throws IOException
    {
        Assume.assumeThat(GradleVersion.version(gradleVersion), Fixtures.isGreaterThanOrEqualTo(GRADLE_53));

        fixtures.arrangeTestProject("setup6-transform");

        String buildLog = fixtures.act(true, "-Ptaskinfo.color=" + pColorEnabled, "tiOrder", "test");
        buildLog = LogXformUtil.normalizeAddresses(LogXformUtil.unixifyLineBreaks(buildLog));

        String expected = TestIoUtil.readFileAndClose(getStreamFor(
            "expected-tree6-transform-order" + (pColorEnabled ? "-color" : "-plain") + ".txt"));
        if (GradleVersion.version(gradleVersion).compareTo(GRADLE_60) < 0) {
            final String ansiReset = "\u001b[m";
            expected = expected.replace(":transformationStep_xxxxxxxxx" + (pColorEnabled ? ansiReset : ""),
                "transformationStep_xxxxxxxxx" + (pColorEnabled ? ansiReset : "") + " ");
        }
        Assert.assertTrue(buildLog.contains(expected));
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    public void testTransformNodesJson()
        throws IOException
    {
        Assume.assumeThat(GradleVersion.version(gradleVersion), Fixtures.isGreaterThanOrEqualTo(GRADLE_53));

        fixtures.arrangeTestProject("setup6-transform");

        fixtures.act(true, "tiJson", "test");

        final String actual = LogXformUtil.normalizeAddresses(TestIoUtil.readFileAndClose(new FileInputStream(
            new File(testProjectDir.getRoot(), "build/taskinfo/taskinfo-test.json"))));
        String expected = TestIoUtil.readFileAndClose(getStreamFor("expected-tree6-transform.json"));
        if (GradleVersion.version(gradleVersion).compareTo(GRADLE_60) < 0) {
            expected = expected.replace(":transformationStep_xxxxxxxxx", "transformationStep_xxxxxxxxx");
        }
        Assert.assertEquals(expected, actual);
    }



    /**
     * Finds the given file and returns an open stream to it. If Gradle >= 7.0, "-g7" is appended before the extension.
     * This is to compensate for changed Gradle output in that version.
     *
     * @param pFilename the original file name (without "-g7")
     * @return the effective input stream
     */
    private InputStream getStreamFor(final String pFilename)
    {
        String filename = pFilename;
        if (GradleVersion.version(gradleVersion).compareTo(GRADLE_67) >= 0) {
            int dotPos = pFilename.indexOf('.');
            Assert.assertTrue(dotPos > 0);
            filename = pFilename.substring(0, dotPos) + "-g7" + pFilename.substring(dotPos);
        }
        return getClass().getResourceAsStream(filename);
    }



    /**
     * In Gradle 3.5.1 or older, there are log messages created between the output of our tasks and the task name.
     * Since we can't know their content, we don't match the task name in these cases. This method cuts it off.
     *
     * @param pLogFragment the log fragment we would actually like to match
     * @param pTaskName the name of the task under test
     * @return the part of the log fragment we can actually expect to match
     */
    private String adjustExpectation(final String pLogFragment, final String pTaskName)
    {
        String result = pLogFragment;
        if (GradleVersion.version(gradleVersion).compareTo(GradleVersion.version("3.5.1")) <= 0) {
            Pattern pattern = Pattern.compile(":" + Pattern.quote(pTaskName) + "\\s*$");
            Matcher matcher = pattern.matcher(pLogFragment);
            if (matcher.find()) {
                result = pLogFragment.substring(0, matcher.start(0));
            }
        }
        return result;
    }
}
