/*
 * Copyright 2020 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.util;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import org.barfuin.gradle.taskinfo.TaskInfoDto;
import org.barfuin.gradle.taskinfo.tasks.AbstractInfoTask;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.internal.TaskInternal;
import org.gradle.api.internal.project.taskfactory.TaskIdentity;
import org.gradle.util.GradleVersion;


/**
 * Abstracts from changing Gradle classes about internal task nodes. For example,
 * <code>org.gradle.execution.plan.TaskNode</code> used to be <code>org.gradle.execution.taskgraph.TaskInfo</code> in
 * older Gradle. We just want some data out of them.
 */
@NotThreadSafe
public class TaskNodeHolder
{
    private final Project project;

    private final Object delegate;

    private final Task task;

    private String identity = null;



    public TaskNodeHolder(@Nonnull final Project pProject, @Nonnull final Object pDelegate)
    {
        project = Objects.requireNonNull(pProject, "pProject was null");
        delegate = Objects.requireNonNull(pDelegate, "pDelegate was null");
        if (ReflectUtil.hasField(pDelegate, "task")) {
            task = (Task) ReflectUtil.readField(delegate, "task field", Collections.singletonList("task"));
        }
        else {
            task = null;
        }
    }



    @CheckForNull
    public Task getTask()
    {
        return task;
    }



    public boolean isOneOfOurTasks()
    {
        if (getTask() != null) {
            return getTask() instanceof AbstractInfoTask;
        }
        return false;
    }



    @Nonnull
    public String getIdentity()
    {
        if (identity != null) {
            return identity;
        }

        String result = null;
        if (getTask() != null) {
            // delegate is a TaskNode
            result = getTask().getPath();
        }
        else {
            final String idHash = Integer.toHexString(System.identityHashCode(delegate));
            if (ReflectUtil.hasField(delegate, "action")) {
                // delegate is an ActionNode
                Object action = ReflectUtil.readField(delegate, "work node action",
                    Collections.singletonList("action"));
                Project owningProject = (Project) ReflectUtil.callMethodIfPresent(action, "getProject");
                if (owningProject != null) {
                    result = getPathWithSeparator(owningProject);
                }
                else {
                    owningProject = (Project) ReflectUtil.callMethodIfPresent(action, "getOwningProject");
                    if (owningProject != null) {
                        result = getPathWithSeparator(owningProject);
                    }
                    else {
                        result = "";
                    }
                }
                result += "workNodeAction" + idHash;
            }
            else {
                // delegate is a TransformationNode
                // Transformation nodes do not require project state (so says the Gradle source code), so if we can't
                // find a project, we can leave it out if it is otherwise ensured that we are a transformation node
                // (before 6.0, it is impossible to find out the project, because no 'owningProject').
                Object projectInternal = ReflectUtil.callMethodIfPresent(delegate, "getOwningProject");
                result = (String) ReflectUtil.callMethodIfPresent(projectInternal, "getPath");
                if (result != null) {
                    if (!Project.PATH_SEPARATOR.equals(result)) {
                        result += Project.PATH_SEPARATOR;
                    }
                }
                else if (isTransformNodeForSure(delegate)) {
                    result = "";
                }
                if (result != null) {
                    result += "transformationStep" + idHash;
                }
            }
        }

        if (result == null) {
            final String idHash = Integer.toHexString(System.identityHashCode(delegate));
            result = "unknownNode" + idHash;
            project.getLogger().warn("WARNING: While working with Gradle's internal execution queue and task "
                + "dependencies, we encountered a node of type '" + delegate.getClass().getName() + "' which is "
                + "either an unknown type, or does not have expected fields. When this happens, it normally means you "
                + "are using a new version of Gradle which is not supported by this plugin yet. You are using "
                + GradleVersion.current().toString()
                + ". Open an issue at https://gitlab.com/barfuin/gradle-taskinfo/-/issues.");
        }
        identity = result;
        return result;
    }



    private String getPathWithSeparator(final Project pProject)
    {
        String result = pProject.getPath();
        if (!Project.PATH_SEPARATOR.equals(result)) {
            result += Project.PATH_SEPARATOR;
        }
        return result;
    }



    private boolean isTransformNodeForSure(@Nonnull final Object pObject)
    {
        final Class<?> transformNodeClass = ReflectUtil.loadClassIfPossible(
            "org.gradle.api.internal.artifacts.transform.TransformationNode");
        return transformNodeClass != null && transformNodeClass.isAssignableFrom(delegate.getClass());
    }



    public String getDisplayName()
    {
        return getTask() != null ? getTask().getName() : delegate.toString();
    }



    @Nonnull
    public Class<?> getType()
    {
        Class<?> result = delegate.getClass();
        if (getTask() != null) {
            result = readType(getTask());
        }
        return result;
    }



    @Nonnull
    private Class<?> readType(@Nonnull final Task pTask)
    {
        Class<?> result = null;
        try {
            try {
                final TaskIdentity<?> taskIdentity = ((TaskInternal) pTask).getTaskIdentity();
                result = taskIdentity.type;
            }
            catch (NoSuchMethodError | ClassCastException e) {
                if (ReflectUtil.hasField(pTask, "publicType")) {
                    Class<?> publicType = (Class<?>) ReflectUtil.readField(pTask,
                        "type information", Collections.singletonList("publicType"));
                    result = publicType;
                }
            }
        }
        catch (RuntimeException | LinkageError e) {
            // fine, we'll use the fallback below
        }
        if (result == null) {
            result = pTask.getClass();
        }
        return result;
    }



    @Nonnull
    public List<TaskNodeHolder> getDependencySuccessors()
    {
        if (ReflectUtil.hasField(delegate, "dependencySuccessors")) {
            Iterable<?> dependencies = (Iterable<?>) ReflectUtil.readField(delegate, "dependency successors",
                Collections.singletonList("dependencySuccessors"));
            return wrapIntoList(dependencies);
        }
        return Collections.emptyList();
    }



    @Nonnull
    public List<TaskNodeHolder> getFinalizers()
    {
        // Only TaskNodes have the 'finalizers' field. The other forms of Nodes don't.
        if (ReflectUtil.hasField(delegate, "finalizers")) {
            Iterable<?> finalizerTasks = (Iterable<?>)
                ReflectUtil.readField(delegate, "finalizer tasks", Collections.singletonList("finalizers"));
            return wrapIntoList(finalizerTasks);
        }
        return Collections.emptyList();
    }



    private List<TaskNodeHolder> wrapIntoList(final Iterable<?> pNodes)
    {
        List<TaskNodeHolder> result = StreamSupport.stream(pNodes.spliterator(), false)
            .map(node -> new TaskNodeHolder(project, node))
            .collect(Collectors.toList());
        return result;
    }



    @Nonnull
    public TaskInfoDto asTaskInfoDto(final boolean pFinalizer, final boolean pColorEnabled)
    {
        final TaskInfoDto result = new TaskInfoDto(getIdentity(), pFinalizer, getTask() != null, pColorEnabled);
        result.setName(getDisplayName());
        result.setGroup(getTask() != null ? getTask().getGroup() : null);
        result.setType(getType().getName());
        return result;
    }
}
