/*
 * Copyright 2020 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.gradle.api.GradleException;
import org.gradle.api.execution.TaskExecutionGraph;
import org.gradle.util.GradleVersion;


/**
 * Utility methods to access Gradle properties on objects reflectively.
 */
public final class ReflectUtil
{
    private ReflectUtil()
    {
        // utility class
    }



    @Nonnull
    public static Object readField(@Nonnull final Object pObject, final String pDescription,
        @Nonnull final List<String> pFieldNames)
    {
        final String objType = pObject.getClass().getName();
        for (String fieldName : pFieldNames) {
            try {
                Object result = FieldUtils.readField(pObject, fieldName, true);
                if (result != null) {
                    return result;
                }
                // we found the field, but it had no content
            }
            catch (IllegalArgumentException e) {
                // in commons-lang3, this means "field not found"
            }
            catch (IllegalAccessException | RuntimeException e) {
                throwGradleException(pDescription, objType, e);
            }
        }
        throwGradleException(pDescription, objType, null);
        return null;
    }



    public static boolean hasField(final Object pObject, final String pFieldName)
    {
        try {
            return FieldUtils.getField(pObject.getClass(), pFieldName, true) != null;
        }
        catch (RuntimeException e) {
            return false;
        }
    }



    @Nonnull
    public static Object getExecutionPlan(@Nonnull final TaskExecutionGraph pTaskGraph)
    {
        return readField(pTaskGraph, "task execution plan", Arrays.asList("taskExecutionPlan", "executionPlan"));
    }



    /**
     * Call the given parameterless method on the given object if it exists and return the result.
     *
     * @param pObject an object
     * @param pMethodName the name of a parameterless method
     * @return the method's return value. A result of <code>null</code> may mean that the method did not exist, or
     * its return value was <code>null</code>.
     */
    @CheckForNull
    public static Object callMethodIfPresent(@Nullable final Object pObject, @Nonnull final String pMethodName)
    {
        Object result = null;
        if (pObject != null) {
            try {
                Method method = MethodUtils.getAccessibleMethod(pObject.getClass(), pMethodName);
                if (method != null) {
                    result = method.invoke(pObject);
                }
            }
            catch (IllegalAccessException | InvocationTargetException | RuntimeException e) {
                // ignore
            }
        }
        return result;
    }



    @CheckForNull
    public static Class<?> loadClassIfPossible(@Nonnull final String pFqcn)
    {
        Class<?> result = null;
        try {
            result = Class.forName(pFqcn);
        }
        catch (ClassNotFoundException e) {
            // ignore - this may happen due to different Gradle versions
        }
        return result;
    }



    private static void throwGradleException(final String pDescription, final String pType, final Throwable pCause)
    {
        throw new GradleException("Failed to access " + pDescription + " in Gradle on object of type '"
            + pType + "'. When this happens, it normally means you are using a new version of Gradle which is not "
            + "supported by this plugin yet. You are using " + GradleVersion.current().toString()
            + ". Open an issue at https://gitlab.com/barfuin/gradle-taskinfo/-/issues.", pCause);
    }
}
