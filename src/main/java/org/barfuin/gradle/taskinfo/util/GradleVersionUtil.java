/*
 * Copyright 2020 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.util;

import org.barfuin.gradle.taskinfo.GradleTaskInfoPlugin;
import org.gradle.tooling.UnsupportedVersionException;
import org.gradle.util.GradleVersion;


/**
 * Some utility methods about checking the Gradle version in order to establish which features are supported.
 */
public final class GradleVersionUtil
{
    private static final GradleVersion GRADLE_34 = GradleVersion.version("3.4");

    private static final GradleVersion GRADLE_49_RC_1 = GradleVersion.version("4.9-rc-1");



    private GradleVersionUtil()
    {
        // utility class
    }



    public static boolean isMinimumSupportedVersion()
    {
        return GradleVersion.current().compareTo(GRADLE_34) >= 0;
    }



    public static String getMinimumSupportedVersion()
    {
        return GRADLE_34.getVersion();
    }



    public static void validateMinimumGradleVersion(final String pTaskName)
    {
        if (!isMinimumSupportedVersion()) {
            throw new UnsupportedVersionException("The '" + pTaskName + "' task defined by the '"
                + GradleTaskInfoPlugin.PLUGIN_ID + "' plugin requires at least Gradle " + getMinimumSupportedVersion()
                + " to be run.");
        }
    }



    public static boolean isDeferredConfigSupported()
    {
        return GradleVersion.current().compareTo(GRADLE_49_RC_1) >= 0;
    }



    public static String getDeferredConfigMinVersion()
    {
        return GRADLE_49_RC_1.getVersion();
    }
}
