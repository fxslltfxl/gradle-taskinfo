/*
 * Copyright 2020 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

import org.barfuin.gradle.taskinfo.tasks.AbstractInfoTask;
import org.barfuin.gradle.taskinfo.tasks.TaskInfoJsonTask;
import org.barfuin.gradle.taskinfo.tasks.TaskInfoOrderTask;
import org.barfuin.gradle.taskinfo.tasks.TaskInfoTreeTask;
import org.barfuin.gradle.taskinfo.util.GradleVersionUtil;
import org.barfuin.gradle.taskinfo.util.TaskNodeHolder;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.execution.TaskExecutionGraph;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.TaskProvider;


/**
 * The main class of the Gradle TaskInfo Plugin.
 */
public class GradleTaskInfoPlugin
    implements Plugin<Project>
{
    public static final String PLUGIN_ID = "org.barfuin.gradle.taskinfo";

    public static final String TASKINFO_TASK_NAME = "tiTree";

    public static final String TASKINFO_JSON_TASK_NAME = "tiJson";

    public static final String TASKINFO_ORDERED_TASK_NAME = "tiOrder";

    public static final String TASKINFO_EXT_NAME = "taskinfo";



    public void apply(@Nonnull final Project pProject)
    {
        final TaskContainer tasks = pProject.getTasks();
        pProject.getExtensions().create(TASKINFO_EXT_NAME, TaskInfoExtension.class);

        if (GradleVersionUtil.isDeferredConfigSupported()) {
            pProject.getLogger().debug("Using deferred configuration with Gradle "
                + GradleVersionUtil.getDeferredConfigMinVersion() + " or newer");

            TaskProvider<TaskInfoTreeTask> tiTree = tasks.register(TASKINFO_TASK_NAME, TaskInfoTreeTask.class);
            TaskProvider<TaskInfoOrderTask> tiOrd = tasks.register(TASKINFO_ORDERED_TASK_NAME, TaskInfoOrderTask.class);
            TaskProvider<TaskInfoJsonTask> tiJson = tasks.register(TASKINFO_JSON_TASK_NAME, TaskInfoJsonTask.class);

            pProject.getGradle().getTaskGraph().whenReady(executionGraph -> {
                final EntryNodeProvider ep = new EntryNodeProvider(pProject, executionGraph);
                Arrays.asList(tiTree, tiOrd, tiJson).forEach(provider -> {
                    provider.configure(t -> configureEntryNode(ep, Collections.singletonList(t)));
                    provider.configure(t -> disableAllNormalTasks(executionGraph, Collections.singletonList(t)));
                });
            });
        }
        else {
            pProject.getLogger().debug("Deferred configuration unavailable because Gradle older than "
                + GradleVersionUtil.getDeferredConfigMinVersion());

            final List<AbstractInfoTask> ourTasks = new ArrayList<>();
            ourTasks.add(tasks.create(TASKINFO_TASK_NAME, TaskInfoTreeTask.class));
            ourTasks.add(tasks.create(TASKINFO_JSON_TASK_NAME, TaskInfoJsonTask.class));
            ourTasks.add(tasks.create(TASKINFO_ORDERED_TASK_NAME, TaskInfoOrderTask.class));

            pProject.getGradle().getTaskGraph().whenReady(executionGraph -> {
                final EntryNodeProvider ep = new EntryNodeProvider(pProject, executionGraph);
                configureEntryNode(ep, ourTasks);
                disableAllNormalTasks(executionGraph, ourTasks);
            });
        }
    }



    private void disableAllNormalTasks(final TaskExecutionGraph pExecutionGraph, final List<AbstractInfoTask> pOurTasks)
    {
        AbstractInfoTask ourTask = findAnyOfOurTasks(pExecutionGraph, pOurTasks);
        if (ourTask != null) {
            GradleVersionUtil.validateMinimumGradleVersion(ourTask.getName());
            pExecutionGraph.getAllTasks().forEach(task -> {
                if (!(task instanceof AbstractInfoTask)) {
                    task.setEnabled(false);
                }
            });
        }
    }



    private void configureEntryNode(final EntryNodeProvider pEntryNodeProvider,
        final List<AbstractInfoTask> pOurTasks)
    {
        TaskNodeHolder entryNode = pEntryNodeProvider.getEffectiveEntryNode();
        String label = pEntryNodeProvider.getLabel();
        for (AbstractInfoTask ourTask : pOurTasks) {
            ourTask.setEntryNode(entryNode);
            ourTask.setEntryNodeLabel(label);
        }
    }



    @CheckForNull
    private AbstractInfoTask findAnyOfOurTasks(final TaskExecutionGraph pExecutionGraph,
        final List<AbstractInfoTask> pOurTasks)
    {
        AbstractInfoTask result = null;
        for (AbstractInfoTask ourTask : pOurTasks) {
            if (pExecutionGraph.hasTask(ourTask)) {
                result = ourTask;
                break;
            }
        }
        return result;
    }
}
