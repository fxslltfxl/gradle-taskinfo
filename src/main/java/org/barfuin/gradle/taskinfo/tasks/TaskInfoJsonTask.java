/*
 * Copyright 2020 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.tasks;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.barfuin.gradle.taskinfo.TaskInfoDto;
import org.barfuin.gradle.taskinfo.TaskProbe;
import org.barfuin.gradle.taskinfo.util.TaskNodeHolder;
import org.gradle.api.GradleException;
import org.gradle.api.Task;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;


/**
 * Task which gathers the required task info and writes it to a JSON file.
 */
public class TaskInfoJsonTask
    extends AbstractInfoTask
{
    private File outputFile = null;



    public TaskInfoJsonTask()
    {
        super();
        setDescription("Creates a JSON file with task dependencies of a given task, and their task types.");
    }



    @Override
    public void setEntryNode(final TaskNodeHolder pEntryNode)
    {
        super.setEntryNode(pEntryNode);
        final Task entryTask = pEntryNode.getTask();
        outputFile = new File(getProject().getBuildDir(),
            "taskinfo/taskinfo-" + (entryTask != null ? entryTask.getName() : "null") + ".json");
    }



    @Override
    @TaskAction
    public void executeTaskInfo()
    {
        validatePreconditions();
        try {
            Files.createDirectories(outputFile.getParentFile().toPath());
        }
        catch (IOException e) {
            throw new GradleException("Failed to create directory: " + outputFile.getParentFile().getAbsolutePath(), e);
        }

        TaskInfoDto taskInfo = new TaskProbe(getProject(), false).buildHierarchy(getEntryNode());
        outputTaskInfo(taskInfo);
    }



    private void outputTaskInfo(@Nonnull final TaskInfoDto pTaskInfo)
    {
        try {
            new ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(outputFile, pTaskInfo);
        }
        catch (IOException | RuntimeException e) {
            throw new GradleException("Failed to write output JSON file: " + outputFile.getAbsolutePath(), e);
        }
    }



    @OutputFile
    public File getOutputFile()
    {
        return outputFile;
    }
}
