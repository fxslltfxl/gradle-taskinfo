/*
 * Copyright 2020 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.tasks;

import javax.annotation.Nonnull;

import org.barfuin.gradle.taskinfo.TaskInfoDto;
import org.barfuin.gradle.taskinfo.TaskInfoExtension;
import org.barfuin.gradle.taskinfo.TaskProbe;
import org.barfuin.texttree.api.CycleProtection;
import org.barfuin.texttree.api.IdentityScheme;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.fusesource.jansi.Ansi;
import org.gradle.api.tasks.TaskAction;


/**
 * Task which gathers the required task info and shows it as a dependency tree.
 */
public class TaskInfoTreeTask
    extends AbstractInfoTask
{
    public TaskInfoTreeTask()
    {
        super();
        setDescription("Displays task dependencies of a given task as a tree, and their task types.");
    }



    @Override
    @TaskAction
    public void executeTaskInfo()
    {
        validatePreconditions();
        final TaskInfoExtension config = getConfig();

        final TaskProbe taskProbe = buildTaskProbe(config);
        TaskInfoDto taskInfo = taskProbe.buildHierarchy(getEntryNode());

        outputTaskInfo(taskInfo, config);
        if (taskProbe.isNonTaskNodesPresent() && config.isColor()) {
            String msg = Ansi.ansi().a("Nodes in ").fgBrightBlack().a("gray").reset().a(" are not backed by tasks "
                + "but by transformation or action nodes added by Gradle.").toString();
            getLogger().lifecycle(msg);
        }
    }



    public TaskProbe buildTaskProbe(final TaskInfoExtension pConfig)
    {
        return new TaskProbe(getProject(), pConfig.isColor());
    }



    private void outputTaskInfo(final TaskInfoDto pTaskInfo, final TaskInfoExtension pConfig)
    {
        final TreeOptions options = buildTreeOptions(pConfig);
        String tree = TextTree.newInstance(options).render(pTaskInfo);
        getLogger().lifecycle(tree);
    }



    @Nonnull
    private TreeOptions buildTreeOptions(@Nonnull final TaskInfoExtension pConfig)
    {
        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.Aligned);
        options.setEnableDefaultColoring(pConfig.isColor());
        options.setIdentityScheme(IdentityScheme.ByKey);
        options.setCycleProtection(pConfig.isClipped() ? CycleProtection.PruneRepeating : CycleProtection.On);
        options.setCycleAsPruned(true);
        return options;
    }
}
